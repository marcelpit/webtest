package starter.stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.thucydides.core.annotations.BlurScreenshots;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.screenshots.BlurLevel;
import net.thucydides.core.util.EnvironmentVariables;
import starter.login.LoginUser;
import starter.navigation.NavigateTo;

import static org.assertj.core.api.Assertions.assertThat;

public class LoginStepDefinitions {

    @Steps
    NavigateTo navigateTo;
    @Steps
    LoginUser loginUser;
    private EnvironmentVariables environmentVariables;


    @Given("Stacy wants to log into her account")
    public void stacyWantsToLogIntoHetAccount() {
        navigateTo.loginPage();

    }

    @BlurScreenshots(BlurLevel.MEDIUM)
    @When("she enters email and her password")
    public void sheEntersAnd() {
        String password =  EnvironmentSpecificConfiguration.from(environmentVariables)
                .getProperty("login.stacy");
        String emailName =  EnvironmentSpecificConfiguration.from(environmentVariables)
                .getProperty("email.stacy");
        String email = emailName + "@mailinator.com";
        loginUser.withCredentials(email, password);


    }

    @Then("she is logged into her account")
    public void sheIsLoggedIntoHerAccount() {
        assertThat(loginUser.isWelcomeMessage()).isEqualTo("Welcome Stacy.");
        assertThat(loginUser.accountMenuText()).contains("My Account");

    }

    @When("she used an invalid password")
    public void sheUsedAnInvalidPassword() {
        String emailName =  EnvironmentSpecificConfiguration.from(environmentVariables)
                .getProperty("email.stacy");
        String email = emailName + "@mailinator.com";
        loginUser.withCredentials(email, "anInvalidPass");

    }

    @Then("she seen an error message")
    public void sheSeenAnErrorMessage() {

        assertThat(loginUser.getInvalidLoginMessage()).contains("Invalid login");


    }
}

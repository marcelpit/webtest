## Test suite for staples.ca
This project is an automated test suite for Staples.ca

##### Technology
The suite uses Serenity-BDD and Cucumber to automate BDD style scenarios with Selenium.

##### Setup and Running
The project includes the necessary files to run in a Gitlab pipeline.
This Gitlab pipeline uses a Maven-JDK container and a Chromedriver container to run the test.
This configuration for the Pipeline can be found in the .gitlab-ci.yml and serenity.conf files.
The serenity.conf file also includes configurations for running with local webdriver instances or a (local) selenium grid.

For example, you can run the test suite like this:

    clean verify -Denvironment=default

This default configuration will run with included webdriver binaries.

***Attention:*** 
**Some of the login test will not work without the right credentials.
However, the credentials will be added to the Gitlab Pipeline.**

##### Bugs in system under test

[Bug List](./bugs.md)

##### Automation issues
Since we are testing on a live website. The reCAPTCHA can possibly trigger on the registration page (test marked as @Manual)

Possible solutions for this are:
   * Set up test environment without reCAPTCHA
   * Set up environment with reCAPTCHA test keys
   * use (paid) service to solve reCAPTCHA


####Test FLows and Test Cases  
*Features:*

[Login Scenarios](./src/test/resources/features/staples/login/login.feature)

These follow a user login flow. A user goes to the login page and fills in credentials.
We test both a correct and false logins.
With a correct login: The message "Welcome Stacy." shows.
With an incorrect login: The user get an "Invalid login" message

[Registration Scenarios](./src/test/resources/features/staples/register/register.feature)
There are two scenarios. One where the user gives valid credentials and get an email message from Staples.ca.
As soon as the user completes the registration they will be logged into the website.
This scenario however can trigger reCAPTCHA, so we have marked the test @Manual.
See feature file for more info.
The second scenario is a user who tries to register with a very short password. We expect an error message.
However the error message is flawed. See bug list.

[Search Scenarios](./src/test/resources/features/staples/search/search_by_keyword.feature)
We have 3 test cases for the search feature.

* Searching by a regular search term like: `printer `  -> Shows a large list of printers
* Search by a product id: `24342684  `                 -> Shows 1 item
* Search by a larger phrase: `printers on sale `       -> Show a list of printers, but only the ones that are on sale

[Shoppingcart Scenarios](./src/test/resources/features/staples/shoppingcart/shoppingcart.feature)
There are 3 test cases for the shopping cart.
* We add an item from it's product page, and we check it has been added to the cart.
* We add 1 item to the cart. Then we remove it and check that the cart is empty.
* We add 2 items to the cart. Then we remove 1 and check that it has been removed correctly.
There is anther complicated scenario for adding "sugested" items to the cart and removing those. This scenario has a bug and has not been automated.
See BUG 1 on the bug list.

[Checkout Scenarios](./src/test/resources/features/staples/checkout/checkout.feature)
There are 2 scenarios
We have 1 item in the cart. We go to checkout, fill in shipping details:
* email: "stacystetson@mailinator.com"
* name:"Stacy"
* last name:"Stetson"
* address: "123 The Street"
* City: "Toronto"
* Province: "Ontario"
* Zipcode: "P0K1P8", 
* Phone number:"7056534672"

This will allow user to proceed to payment options.

In the second scenario the user has an invalid email address
Therefore an error message is displayed, alerting the user to this mistake.




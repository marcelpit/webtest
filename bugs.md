##### Discovered bugs in System under test
**BUG 1:**

When I add a "sugested item" for the 2nd time. The cart does not respresent the situation correctly****

Reproduction steps:
 * Add an item to the shopping cart.
 * Also add a suggested item to the cart
 * Continue shopping and add the same suggested item to the cart for a 2nd time. But this time add it from that items product page.
 * Look in the cart. Now it is very unclear that the extra item has been added to the cart 2 times.
 * The cart does not list 3 items. The 2nd extra item is not visible at all.
 
 ![bug screenshot1](./screenshots/3_items_2_visible.PNG)
  
  Luckily the costs are still calculated correctly.
  Consequences for the checkout process are unclear.
  
 Additional problems:   
 * I can uncheck the extra item from the cart. It does get removed correctly. But the remaining extra item is still not listed.
 * It is impossible to remove this 2nd extra item from the cart.
 
  ![bug screenshot2](./screenshots/after_Delete_2_items_1_visible.PNG)
  
  
  
**BUG 2:**
    When I try to register with a very short password, the error message is unclear and not well formatted.  
  
  ![bug screenshot3](./screenshots/Register_password_error_message.png)
  
  
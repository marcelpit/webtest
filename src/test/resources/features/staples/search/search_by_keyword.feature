Feature: Search for items

  Scenario: Searching for an item
    Given tom is on the Staples home page
    When he searches for "printer"
    Then he sees the products result page for "printer"
    And some suggested items will be listed

  Scenario: Searching by ID
    Given tom is on the Staples home page
    When he searches by "24342684"
    Then only 1 item is listed

  Scenario: Searching for items on sale
    Given tom is on the Staples home page
    When he searches for "printers on sale"
    Then he sees only items that are on sale

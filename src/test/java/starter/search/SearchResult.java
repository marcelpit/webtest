package starter.search;

import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.core.steps.UIInteractionSteps;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class SearchResult extends UIInteractionSteps {
    @FindBy(css = "div.search-header")
    WebElementFacade resultTitle;

    @FindBy(css = "div.ais-facet--item[data-algolia-title=sale]")
    WebElementFacade onSaleFilter;

    @FindBy(css = "div.ais-stats-container.ais-stats > div")
    WebElementFacade resultsFoundSummary;

    //Todo remove deprecated method. Maybe remove wait compeltely and let global Wait handle it.
    public List<WebElementFacade> items() {
        return withTimeoutOf(10, TimeUnit.SECONDS).findAll(SearchResultList.RESULT_ITEMS);

    }

    public String pageTitle() {
        return waitFor(resultTitle).getText();

    }

    /**
     * Finds the number for all items that are found.
     * Not just the items that are displayed on the page.
     *
     * @return the total amount of items found
     */
    public int totalAmountOfResultsFound() {
        //Use Regex to only find the total amount of items found from the results found summary.
        Pattern pattern = Pattern.compile("(\\d+)(?!.*\\d)");
        Matcher matcher = pattern.matcher(resultsFoundSummary.getText());
        matcher.find();
        String text = matcher.group();
        return Integer.parseInt(text);
    }

    /**
     * Check how many items of the found results are on sale. By using th "On Sale" filter.
     * @return int The amount of found products that are on sale.
     */
    public int amountOfItemsOnSale() {
        String onSaleFilterCounterText = onSaleFilter.find(By.cssSelector("span.ais-facet--count")).getText().replaceAll("[^0-9.]", "");
        return Integer.parseInt(onSaleFilterCounterText);
    }
}

package starter.stepdefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import starter.cart.Cart;
import starter.checkout.Checkout;
import starter.navigation.NavigateTo;
import starter.product.Product;

import static org.assertj.core.api.Assertions.assertThat;

public class ShoppingCartStepDefinitions {
    @Steps
    NavigateTo navigateTo;
    @Steps
    Product product;
    @Steps
    Cart cart;
    @Steps
    Checkout checkout;

    @Given("Stacy has found a product she likes")
    public void stacyHasFoundAProductSheLikes() {
        navigateTo.specificPrinterProductPage();
    }

    @When("she adds the product to the shopping cart")
    public void sheAddsTheProductToTheShoppingCart() {
        product.addToCart();
        product.viewCart();
    }

    @Then("she can see the product in her cart")
    public void sheCanSeeTheProductInHerCart() {
        assertThat(cart.containsNrOfProducts()).isEqualTo(1);
        assertThat(cart.containsProduct("2911666")).isTrue();
    }

    @Given("Stacy has added a product to the shopping cart")
    public void stacyHasAddedAProductToTheShoppingCart() {
        navigateTo.specificPrinterProductPage();
        product.addToCart();
        product.viewCart();
    }

    @When("she/he removes the/an item from the shopping cart")
    public void sheRemovesTheItemFromTheShoppingCart() {
        cart.removeItemwithID("2911666");
    }

    @Then("the shopping cart does not show the item anymore")
    public void theShoppingcartDoesNotShowTheItemAnymore() {
        assertThat(cart.getEmptyCartText()).isEqualTo("You currently have no products in your cart. Start shopping.");
    }

    @And("she/he adds another product")
    public void sheAddsAnotherProduct() {
        navigateTo.product("394351-en-hp-office-copy-paper-20-lb-8-12-x-11-ream");
        product.addToCart();
        product.viewCart();
    }


    @When("she/he wants to checkout")
    public void sheWantsToCheckout() {
        cart.clickCheckoutButton();

    }

    @Then("she can fill in her shipping details")
    public void sheCanFillInHerShippingDetails() {
        checkout.fillInShippingDetails("stacystetson@mailinator.com", "Stacy", "Stetson", "123 The Street", "Toronto", "Ontario", "P0K1P8", "7056534672");
    }

    @And("she/he can proceed to pay")
    public void sheCanProceedToPay() {
        checkout.clickPaymentButton();

    }

    @And("And the creditcard option is available")
    public void andTheCreditcardOptionIsAvailable() {
        assertThat(checkout.checkForCC()).contains("Credit card");
    }

    @Then("she can fill in her incomplete shipping details")
    public void sheCanFillInHerIncompleteShippingDetails() {
        checkout.fillInShippingDetails(".com", "Stacy", "Stetson", "123 The Street", "Toronto", "Ontario", "P0K1P8", "7056534672");
    }

    @And("she can not proceed to pay")
    public void sheCanNotProceedToPay() {
        checkout.clickPaymentButton();
        assertThat(checkout.getShippingErrormessage()).contains("Please enter/select a valid shipping address before proceeding to payment");
    }
}

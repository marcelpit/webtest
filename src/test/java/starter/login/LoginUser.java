package starter.login;

import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.steps.UIInteractionSteps;
import org.openqa.selenium.By;

public class LoginUser extends UIInteractionSteps {

    By emailField = By.id("customer_email");
    By passwordField = By.id("customer_password");
    By loginSignInButton = By.xpath("//button[contains(text(),'Sign In')]");
    By welcomeMessage = By.cssSelector("section#customer_order_history > h1.h3");
    By accountMenu = By.id("account-drop-down__tab");
    By invalidLoginErrorMessage = By.cssSelector("form#customer_login > div.errors");



    public void withCredentials(String email, String password) {
        find(emailField).type(email);
        find(passwordField).type(password);
        find(loginSignInButton).click();
    }

    public String isWelcomeMessage() {
        return find(welcomeMessage).getText();

    }
    public String accountMenuText(){
        return find(accountMenu).getText();
    }

    public String getInvalidLoginMessage() {
    return find(invalidLoginErrorMessage).getText();


    }
}

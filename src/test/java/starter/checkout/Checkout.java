package starter.checkout;

import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.core.steps.UIInteractionSteps;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class Checkout extends UIInteractionSteps {

    @FindBy(css = "input[type='email']")
    WebElementFacade emailField;
    @FindBy(css = "div.FormTextField__InputWrapper > input[placeholder='First Name']")
    WebElementFacade accountFirstName;
    @FindBy(css = "div.FormTextField__InputWrapper  > [placeholder='Last Name']")
    WebElementFacade accountLastName;
    @FindBy(name = "shipping_address[first_name]")
    WebElementFacade shippingNameField;
    @FindBy(name = "shipping_address[last_name]")
    WebElementFacade shippingLastNameField;
    @FindBy(name = "shipping_address[address1]")
    WebElementFacade shippingAddressField;
    @FindBy(name = "shipping_address[city]")
    WebElementFacade cityField;
    @FindBy(name = "shipping_address[province]")
    WebElement provinceField;
    @FindBy(name = "shipping_address[zip]")
    WebElementFacade zipCodeField;
    @FindBy(name = "shipping_address[phone]")
    WebElementFacade phoneField;
    @FindBy(xpath = "//button[contains(text(),'Proceed to Payment')]")
    WebElementFacade proceedToPaymentButton;
    @FindBy(css = "label[for='pmt-credit-card']")
    WebElementFacade creditCardLabel;
    @FindBy(css = "div.Alert__Content")
    WebElementFacade emailErrormessage;

    public void fillInShippingDetails(String email, String name, String lastName, String street, String city, String province, String zipCode, String phone) {
        //wait till at least the button is visible
        waitFor(proceedToPaymentButton);
        //go to the shipping details frame
        getDriver().switchTo().frame("widget_69cc8403-c2e7-40b0-b5e0-f1d0572821e7_shipping_widget");

        accountFirstName.type(name);
        accountLastName.type(lastName);
        emailField.type(email);
        shippingNameField.type(name);
        shippingLastNameField.type(lastName);
        shippingAddressField.type(street);
        cityField.type(city);

        Select provinceSelect = new Select(provinceField);
        provinceSelect.selectByValue(province);
        zipCodeField.type(zipCode);
        phoneField.type(phone);
        //Back to parentframe
        getDriver().switchTo().parentFrame();

    }

    public void clickPaymentButton() {
        proceedToPaymentButton.waitUntilClickable().click();
    }

    public String checkForCC() {
        return creditCardLabel.getText();
    }

    public String getShippingErrormessage() {
        getDriver().switchTo().frame("widget_69cc8403-c2e7-40b0-b5e0-f1d0572821e7_shipping_widget");
        String errorMessage = emailErrormessage.getText();
        getDriver().switchTo().parentFrame();
        return errorMessage;
    }
}

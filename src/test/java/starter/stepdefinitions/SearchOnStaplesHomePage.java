package starter.stepdefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import starter.navigation.NavigateTo;
import starter.search.SearchFor;
import starter.search.SearchResult;

import static org.assertj.core.api.Assertions.assertThat;

public class SearchOnStaplesHomePage {
    @Steps
    NavigateTo navigateTo;

    @Steps
    SearchFor searchFor;

    @Steps
    SearchResult searchResult;


    @Given("^(?:.*) is on the Staples home page")
    public void i_am_on_the_Newegg_home_page() {
        navigateTo.staplesHomePage();
    }

    @When("he/she searches for/by {string}")
    public void i_search_for(String term) {
        searchFor.term(term);
    }

    @And("some suggested items will be listed")
    public void someSuggestedItemsWillBeListed() {
        assertThat(searchResult.items()).isNotEmpty();
    }

    @Then("he sees the products result page for {string}")
    public void heSeesAResultPageFor(String term) {
        assertThat(searchResult.pageTitle()).containsIgnoringCase(term);

    }

    @Then("only {int} item is listed")
    public void onlyItemIsListed(int amount) {
        assertThat(searchResult.items()).hasSize(amount);
    }

    @Then("he sees only items that are on sale")
    public void heSeesOnlyItemsThatAreOnSale() {
        assertThat(searchResult.totalAmountOfResultsFound()).isEqualTo(searchResult.amountOfItemsOnSale());

    }
}

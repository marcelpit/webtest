Feature: Buy the items in the cart by checking out

  Scenario: Checkout with items in the cart and valid shipping details
    Given Stacy has added a product to the shopping cart
    When she wants to checkout
    Then she can fill in her shipping details
    And she can proceed to pay
    And And the creditcard option is available


  Scenario: Checkout with items in the cart but using an invalid email address
    Given Stacy has added a product to the shopping cart
    When she wants to checkout
    Then she can fill in her incomplete shipping details
    And she can not proceed to pay





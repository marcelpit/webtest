package starter.navigation;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.annotations.NamedUrl;
import net.thucydides.core.annotations.NamedUrls;

@DefaultUrl("/products")
@NamedUrls(
        {
                @NamedUrl(name = "open.product", url = "https://www.staples.ca/products/{1}")
        }
)
public class ProductPage extends PageObject {


}

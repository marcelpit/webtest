package starter.registration;

import net.serenitybdd.core.steps.UIInteractionSteps;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.By;
import starter.navigation.RegistrationPage;

public class RegistrationForm extends UIInteractionSteps {

    By firstNameField = By.id("first_name");
    By lastNameField = By.id("last_name");
    By emailField = By.id("email");
    By passwordField = By.id("password");
    By submitButton = By.xpath("//button[contains(text(),'Create Account')]");
    By captchaCheckBox = By.id("recaptcha-anchor");
    By captchaSubmitButton = By.cssSelector("input.shopify-challenge__button.btn[type=submit]");
    By errorMessage = By.cssSelector("div.errors");



    @Step
    public void FillOut(String name, String lastname, String email, String password) {
        find(firstNameField).type(name);
        find(lastNameField).type(lastname);
        find(emailField).type(email);
        find(passwordField).type(password);
    }

    public void submit() {
    find(submitButton).click();

    }

    public void dealWithCaptcha() {
        find(captchaCheckBox).click();
        find(captchaSubmitButton).click();
    }


    public String getErrorMessage() {
        return find(errorMessage).getText();
    }
}

package starter.search;

import org.openqa.selenium.By;

class SearchResultList {
    static By RESULT_TITLES = By.cssSelector("div.ais-hits--item");
    static By RESULT_ITEMS = By.cssSelector("div.ais-hits--item");

}

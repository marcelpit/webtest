package starter.navigation;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.Step;

public class NavigateTo extends PageObject{

    ProductPage productPage;
    StaplesHomePage staplesHomePage;
    RegistrationPage registrationPage;
    LoginPage loginPage;

    @Step("Open the Newegg home page")
    public void staplesHomePage() {
        staplesHomePage.open();
    }

    @Step("Open printer product page")
    public void specificPrinterProductPage() {
        productPage.open("open.product", PageObject.withParameters("2911666-en-hp-envy-5055-all-in-one-inkjet-printer-m2u85aa2l"));
    }

    @Step("Open product page")
    public void product(String productLink) {
        productPage.open("open.product", PageObject.withParameters(productLink));
    }
    @Step("Open registration page")
    public void registrationPage() {
        registrationPage.open();
    }

    @Step("Open login page")
    public void loginPage() {
        loginPage.open();
    }


    public void url(String url) {
        getDriver().get(url);

    }
}

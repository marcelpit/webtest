package starter.cart;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;

import java.util.List;

@DefaultUrl("/cart")
public class Cart extends PageObject {
    By cartItem = By.cssSelector("div.cart__item-description");
    By emptyCart = By.cssSelector("#shopify-section-cart > div:nth-child(4) > div");
    By checkoutButton = By.name("checkout");

    public boolean containsProduct(String productId) {
        return itemsInCart().stream()
                .map(WebElementFacade::getText)
                .anyMatch(text -> text.contains("Item: " + productId));
    }

    public List<WebElementFacade> itemsInCart() {
        return waitForRenderedElements(cartItem).findAll(cartItem);
    }

    public void removeItemwithID(String productId) {
        WebElementFacade itemToDelete = itemsInCart().stream().filter(item -> item.getText().contains(productId)).findAny().get();
        itemToDelete.find(By.xpath("//following::a[contains(text(),'Remove Item')]")).click();
    }

    public String getEmptyCartText() {
        return find(emptyCart).getText();
    }

    public int containsNrOfProducts() {
        return findAll(cartItem).size();

    }

    public void clickCheckoutButton() {
        find(checkoutButton).waitUntilClickable().click();

    }
}


Feature: Register a user for a staples.ca account
  #Test are flaky, since they can trigger reCAPTCHA
  #Suggested solutions:
  #     - Set up test environment without reCAPTCHA
  #     - Set up environment with reCAPTCHA test keys
  #      - use (paid) service to solve reCAPTCHA
  @Manual
  Scenario: User registers a Staples.ca account
    Given Joe wants to register for an account
    When he enters his correct details
    Then he will be logged in
    And he will receive an email

  @Issue:BUG-2
  Scenario: User registers a Staples.ca account with invalid password length
    Given Joe wants to register for an account
    When he enters a very short password
    Then he gets a clear error message


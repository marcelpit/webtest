Feature: User are able to login to their Staples.ca account

  Scenario: User logs in to website using valid credentials
    Given Stacy wants to log into her account
    When she enters email and her password
    Then she is logged into her account


  Scenario: User logs in to website using INVALID credentials
    Given Stacy wants to log into her account
    When she used an invalid password
    Then she seen an error message



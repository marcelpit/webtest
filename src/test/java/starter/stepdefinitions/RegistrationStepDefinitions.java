package starter.stepdefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Pending;
import net.thucydides.core.annotations.Steps;
import starter.cart.Cart;
import starter.navigation.NavigateTo;

import starter.product.Product;
import starter.registration.RegistrationForm;

import java.sql.DriverManager;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.allOf;
import static org.assertj.core.api.Assertions.assertThat;

public class RegistrationStepDefinitions {
    @Steps
    NavigateTo navigateTo;
    @Steps
    RegistrationForm registrationForm;


    @Given("Joe wants to register for an account")
    public void joeWantsToRegisterForAnAccount() {
        navigateTo.registrationPage();
    }

    @When("he enters his correct details")
    public void heEntersHisCorrectDetails() {
        registrationForm.FillOut("Joe", "Schmoe", "joe2000@mailinator.com","SafePass");
        registrationForm.submit();
    }


    @Then("he will receive an email")
    public void heWillRecieveAnEmail() {
       navigateTo.url("https://www.mailinator.com/v3/index.jsp?zone=public&query=joe2000#/#inboxpane");
    }

    @Pending
    @Then("he will be logged in")
    public void heWillBeLoggedIn() {
        //TODO
    }

    @When("he enters a very short password")
    public void heEntersAShortPassword() {
        registrationForm.FillOut("Joe", "Schmoe", "joey@mailinator.com","bad");
        registrationForm.submit();
    }

    @Then("he gets a clear error message")
    public void heGetsAClearErrorMessage() {
        //Words we need to see in the password message.
        List<String> shortPasswordMessageWords = Arrays.asList("password", "short");
        assertThat(registrationForm.getErrorMessage()).contains(shortPasswordMessageWords).doesNotContain("count");

    }

}

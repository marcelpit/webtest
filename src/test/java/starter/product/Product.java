package starter.product;


import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.core.steps.UIInteractionSteps;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;

public class Product extends UIInteractionSteps {



    @FindBy(css = "button[product-id]")
    WebElementFacade addToCartButton;

    @FindBy(css = "div.modal-cart__ctas.modal-cart-ctas a[href='/cart']")
    WebElementFacade viewCartButton;

    @FindBy(css="div.Modal button.button--submit")
    List<WebElementFacade> addExtraItemToCartButton;
    //By addExtraItemToCartButton =  By.cssSelector("div.Modal button.button--submit");


    @Step("Add product to cart")
    public void addToCart() {
        addToCartButton.waitUntilClickable().click();
    }

    @Step("View contents of the cart")
    public void viewCart() {
        waitFor(viewCartButton).waitUntilClickable().click();
    }

    public void addExtraProduct() {
        addExtraItemToCartButton.get(0).click();
    }
}
